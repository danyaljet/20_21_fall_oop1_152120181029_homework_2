#include <iostream>
#include <string>
using namespace std;

int main() {
	string a, b, c;
	char temp;

	cin >> a >> b;
	c = a + b;

	int sizeA = a.size(), sizeB = b.size();

	temp = a[0];
	a[0] = b[0];
	b[0] = temp;

	cout << sizeA << " " << sizeB << endl;
	cout << c << endl;
	cout << a << " " << b << endl;

	return 0;
}