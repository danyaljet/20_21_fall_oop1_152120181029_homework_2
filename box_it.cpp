#include<iostream>

using namespace std;

class Box {
public:
	Box();
	Box(int length, int breadth, int height);
	Box(Box& B);

	void setL(int length) {
		l = length;
	}
	void setB(int breadth) {
		b = breadth;
	}
	void setH(int height) {
		h = height;
	}
	int getL() {
		return l;
	}
	int getB() {
		return b;
	}
	int getH() {
		return h;
	}
	long long CalculateVolume() {
		long long volume = (long long)l * b * h;
		return volume;
	}
	friend bool operator < (Box& X, Box& Y);

	friend ostream& operator << (ostream& outPut, Box& B);

private:
	long l, b, h;

};

Box::Box() {
	l = 0, b = 0, h = 0;
}

Box::Box(int length, int breadth, int height) {
	l = length;
	b = breadth;
	h = height;
}

Box::Box(Box& B) {
	l = B.l;
	b = B.b;
	h = B.h;
}

bool operator < (Box& X, Box& Y) {
	if (X.l < Y.l)
		return true;
	else if (X.b < Y.b && X.l == Y.l)
		return true;
	else if (X.h < Y.h && X.b == Y.b && X.l == Y.l)
		return true;
	else
		return false;
}
ostream& operator << (ostream& outPut, Box& B) {
	outPut << B.l << " " << B.b << " " << B.h;
	return outPut;
}
void check2()
{
	int n;
	cin >> n;
	Box temp;
	for (int i = 0; i < n; i++)
	{
		int type;
		cin >> type;
		if (type == 1)
		{
			cout << temp << endl;
		}
		if (type == 2)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			temp = NewBox;
			cout << temp << endl;
		}
		if (type == 3)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			if (NewBox < temp)
			{
				cout << "Lesser\n";
			}
			else
			{
				cout << "Greater\n";
			}
		}
		if (type == 4)
		{
			cout << temp.CalculateVolume() << endl;
		}
		if (type == 5)
		{
			Box NewBox(temp);
			cout << NewBox << endl;
		}

	}
}

int main()
{
	check2();

	return 0;
}