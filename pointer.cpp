#include <stdio.h>

void update(int* a, int* b) {
    int temp = 0;

    if (*b > *a) {
        temp = *b;
        *b = *a;
        *a = temp;
    }

    int x = *a;
    int y = *b;

    *a = x + y;
    *b = x - y;
}

int main() {
    int a, b;
    int* pa = &a, * pb = &b;

    scanf_s("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);

    return 0;
}