#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int main() {
    
    int rowNumber, questionNumber, temp;

    cin >> rowNumber >> questionNumber;

    int** A;
    A = new int* [rowNumber];

    for (int i = 0; i < rowNumber; i++) {
        int colNumber;
        cin >> colNumber;
        A[i] = new int[colNumber];
        for (int j = 0; j < colNumber; j++) {
            cin >> A[i][j];
        }
    }
    int x, y;
    for (int i = 0; i < questionNumber; i++) {
        cin >> x >> y;
        cout << A[x][y] << endl;
    }
   
    return 0;
}