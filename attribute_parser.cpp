#include <cmath>
#include <cstdio>
#include <map>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

vector<string> Tag_Stack;
map<string, string> attributes;


void insAttribute(string& name, string& value) {
    string a;
    for (string& str : Tag_Stack)
        a += str + ".";     
    a.pop_back();        
    a += "~" + name;   
    attributes[a] = value;

}

int main() {
    int number, question;

    cin >> number >> question;

    for (int i = 0; i < number; ++i) {
        char c; 
        cin >> c; 

        if (cin.peek() == '/') {
            string cN; cin >> cN;
            Tag_Stack.pop_back();
        }
        else {
            string name;
            cin >> name; 

            if (name.back() == '>') {     
                name.pop_back();             
                Tag_Stack.push_back(name);
            }
            else {
                Tag_Stack.push_back(name);

                while(true){
                    string attName, attVal, eq;
                    cin >> attName >> eq >> attVal;
                    if (attVal.back() == '>') {
                        attVal.pop_back();
                        attVal.pop_back();
                        attVal = attVal.substr(1);
                        insAttribute(attName, attVal);
                        break;
                    }
                    else {
                        attVal.pop_back();
                        attVal = attVal.substr(1);
                        insAttribute(attName, attVal);
                    }
                }
            }

        }
    }

    for (int i = 0; i < question; ++i) {
        string query;
        cin >> query;
        if (attributes.find(query) != attributes.end())
            cout << attributes[query] << endl;
        else
            cout << "Not Found!" << endl;
    }

    return 0;
}