#include <sstream>
#include <vector>
#include <iostream>
#include <string>
using namespace std;

vector<int> parseInts(string str) {
    
    int size = 1;
    vector <int> strings;

    for (int i = 0; i < str.length(); i++) {
        if (str[i] == ',')
            size++;
    }

    stringstream s(str);

    char ch;
    int a = 0;

    for (int i = 0; i < size; i++) {
        s >> a >> ch;
        strings.push_back(a);
    }
    return strings;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }

    return 0;
}